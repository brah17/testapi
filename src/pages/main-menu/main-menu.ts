import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MainMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main-menu',
  templateUrl: 'main-menu.html',
})
export class MainMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPage');
  }
  showClients(){
   this.navCtrl.push('ShowClientsPage');
  }
  showCharge(){
    this.navCtrl.push('ShowChargesPage');
  }
  showViolations(){
    this.navCtrl.push('ShowContraventionsPage');

  }
  showMaintenance(){
    this.navCtrl.push('ShowEntretiensPage');

  }
  showParc(){
    this.navCtrl.push('ShowParcsPage');
  }
  showAccident(){
    this.navCtrl.push('ShowAccidentsPage');
  }
  showComptability(){
   this.navCtrl.push('ShowCompatabilitiesPage');
  }
  showSettings(){
   this.navCtrl.push('ShowParametresPage');
  }
  showCalendar(){
    this.navCtrl.push('ShowCalendarsPage');
  }

}
