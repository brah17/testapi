import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {

  env: string = 'dev';
  //mobilePcUrl: string = 'a038fc18.ngrok.io';
  //mobilePcUrl: string = '192.168.0.7';
  mobilePcUrl: string = '127.0.0.1:8000';
  devApiUrl: string = 'http://'+this.mobilePcUrl+'/api'
  //devUrl: string = 'http://' + this.mobilePcUrl + '/bluepenlabs/time-for-life-web/public/api'

  devUrl: string = 'https://bluepenlabs.com/projects/time-for-life-web/public/api'
  prodUrl: string = 'https://www.project.com/api';
  token: string = 'WlpMMXRVQURvZ0JSOG5pVmNHdktRYVBXV0pvRDRwSXAreit6bTE0MTFtVGV4QXhaUEJyYmhIbEZhNTVaZzRGb0c2RE5nQjJHaGZWcTNPQjJLYk1tdVJhWlZrSi85bTVPOXdDVHdPc3hEQmRSd3AzMzdDY3F5N25oZy9jRnFFTU1DQVNCaEpGMUFjbWc2OGhXWm5oRGx1SG9TZThScWwrbXVMVUxCazlLRXlFPQ==';

  constructor(public http: HttpClient) { }

  checkEnv() {
    switch (this.env) {
      case 'dev': return this.devApiUrl;
      case 'prod': return this.prodUrl;
      default: return '';
    }
  }

  static headersConfig() {
    let headers = new HttpHeaders();
    /*headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT'); */
    headers.append("Accept", "applications/json");
    headers.append("Content-Type", "application/json");

    return { headers };
  }

  get(endpoint: string) {
    return this.http.get(this.checkEnv() + '/' + endpoint + '/' + this.token, ApiProvider.headersConfig());
  }

  post(endpoint: string, body: any) {
    body.token = this.token;
    return this.http.post(this.checkEnv() + '/' + endpoint, body, ApiProvider.headersConfig());
      
  }

}
