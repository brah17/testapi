import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowParcsPage } from './show-parcs';

@NgModule({
  declarations: [
    ShowParcsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowParcsPage),
  ],
})
export class ShowParcsPageModule {}
