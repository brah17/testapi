import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowCompatabilitiesPage } from './show-compatabilities';

@NgModule({
  declarations: [
    ShowCompatabilitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowCompatabilitiesPage),
  ],
})
export class ShowCompatabilitiesPageModule {}
