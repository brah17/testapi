import { Injectable } from '@angular/core';
import { AlertController, ModalController, LoadingController } from 'ionic-angular';

@Injectable()
export class AlertApiProvider {

  //error, sucsess, warning
  public loading: any;
  public timeOut: any;
  constructor(public alertCtrl: AlertController, public modalCtrl: ModalController,
    public loadingCtrl: LoadingController) {

  }

  /*myAlertMethod(title: string, message: string, type:string) {
    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "OK",
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    confirm.present();
  }*/

  showErrorModal(title: string, message: string) {
    this.alertCtrl.create( {
      title: title,
      message: message
    }).present();

  }

  showSuccessModal(title: string, message: string){
    this.alertCtrl.create( {
      title: title,
      message: message
    }).present();
  }

  showConfirmationModal(title: string, message: string, data:any){
    this.modalCtrl.create('ConfirmationPage', {
      title: title,
      message: message,
      data:data
    }).present();
  }

  showLoadingAlert(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
    this.loading.present();
  }

  hideLoadingAlert() {
    this.loading.dismiss();
  }

  checkConnexionTimeOut(time:number) {
   this.timeOut = setTimeout(() => {
      this.alertCtrl.create({
        title: 'Attention',
        message: "La connexion a échoué",
        buttons: [
          {
            text: 'Fermer',
            role: 'cancel',
            handler: () => {
              this.hideLoadingAlert();
            }
          }
        ]
      }).present();
    }, time);
  }

  clearTimeOut(){
    clearTimeout(this.timeOut);
  }

}
