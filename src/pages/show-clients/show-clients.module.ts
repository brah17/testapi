import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowClientsPage } from './show-clients';

@NgModule({
  declarations: [
    ShowClientsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowClientsPage),
  ],
})
export class ShowClientsPageModule {}
