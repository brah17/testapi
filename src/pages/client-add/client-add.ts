import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

/**
 * Generated class for the ClientAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-add',
  templateUrl: 'client-add.html',
})
export class ClientAddPage {
  @ViewChild(Slides) slides: Slides;



  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientAddPage');
  }
  goToSlideOne(){
    this.slides.slideTo(0,500);
  }
  goToSlideTwo(){
    this.slides.slideTo(2,500);

  }

}
