import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the UserAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-add',
  templateUrl: 'user-add.html',
})
export class UserAddPage {
  inscriptionForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder:FormBuilder) {
    this.inscriptionForm = formBuilder.group({
      lastName: ['', Validators.compose([Validators.required])],
      firstName: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      postalCode: ['', Validators.compose([Validators.required])],
      pays: ['', Validators.compose([Validators.required])],      
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirmation: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([])]
    });
 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserAddPage');
  }

}
