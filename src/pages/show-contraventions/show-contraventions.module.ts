import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowContraventionsPage } from './show-contraventions';

@NgModule({
  declarations: [
    ShowContraventionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowContraventionsPage),
  ],
})
export class ShowContraventionsPageModule {}
