import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowCalendarsPage } from './show-calendars';

@NgModule({
  declarations: [
    ShowCalendarsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowCalendarsPage),
  ],
})
export class ShowCalendarsPageModule {}
