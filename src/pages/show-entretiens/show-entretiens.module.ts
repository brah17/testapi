import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowEntretiensPage } from './show-entretiens';

@NgModule({
  declarations: [
    ShowEntretiensPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowEntretiensPage),
  ],
})
export class ShowEntretiensPageModule {}
