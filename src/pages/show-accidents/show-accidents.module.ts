import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowAccidentsPage } from './show-accidents';

@NgModule({
  declarations: [
    ShowAccidentsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowAccidentsPage),
  ],
})
export class ShowAccidentsPageModule {}
