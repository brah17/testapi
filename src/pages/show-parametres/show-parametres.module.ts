import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowParametresPage } from './show-parametres';

@NgModule({
  declarations: [
    ShowParametresPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowParametresPage),
  ],
})
export class ShowParametresPageModule {}
