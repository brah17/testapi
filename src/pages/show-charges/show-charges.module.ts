import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowChargesPage } from './show-charges';

@NgModule({
  declarations: [
    ShowChargesPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowChargesPage),
  ],
})
export class ShowChargesPageModule {}
