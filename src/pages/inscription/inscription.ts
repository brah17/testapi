import { AlertApiProvider } from './../../providers/alert-api/alert-api';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the InscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inscription',
  templateUrl: 'inscription.html',
})
export class InscriptionPage {
  inscriptionForm: FormGroup;
  public addressObject = null;
  public token : any;
  public picture: any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder,public api:ApiProvider,public apiAlert:AlertApiProvider
    ) {
      
    this.inscriptionForm = formBuilder.group({
      lastName: ['', Validators.compose([Validators.required])],
      firstName: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      postalCode: ['', Validators.compose([Validators.required])],
      pays: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirmation: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InscriptionPage');
  }
  handleRegister(){
    this.apiAlert.showLoadingAlert('Chargement de donnes');
    this.addressObject={
       zipCode: this.inscriptionForm.value.postalCode,
       address: this.inscriptionForm.value.address,
       country: this.inscriptionForm.value.pays
    }
   
    
   // console.log(btoa(this.picture));
   let pic64=btoa(this.picture);
    let doc ={
      token: this.api.token,
      picture: pic64
    }
    
   // console.log(doc);
    this.api.post('media/uploads',doc).toPromise().then((res) => {
     
      if (res['status'] == 200) {
        let user={
          address: this.addressObject.address+" "+this.addressObject.zipCode+" "+this.addressObject.pays,
          name: this.inscriptionForm.value.firstName+ " " +this.inscriptionForm.value.lastName,      email: this.inscriptionForm.value.email,
          phone: this.inscriptionForm.value.phone,
          password: this.inscriptionForm.value.password,
          gender: this.inscriptionForm.value.gender,
          type: 15,
          link:res['picture']
        }
       // console.log(user.link);
      //  user['link']=link;

        this.api.post('user/add',user).toPromise().then((res) => {
     
          if (res['status'] == 200) {
           // console.log('user add successfully');
            this.apiAlert.hideLoadingAlert();
            this.apiAlert.showSuccessModal("User","Nouveau utilisateur ajouté");
          }
          else{
            this.apiAlert.hideLoadingAlert();
            this.apiAlert.showErrorModal('Problem','Probleme d\'ajouté user');
          }
         
        
        }).catch(error => {
           console.log(error);
        });
      // console.log(link);
      }
      else{
         this.apiAlert.hideLoadingAlert();
         this.apiAlert.showErrorModal('Probleme photo',"verfie la photo importé");
      }
     
    
    }).catch(error => {
      console.log(error);
    });
   
   

   
  
    //if()
  }
  
  openFileBrowser(event :any){ 
    event.preventDefault(); 
    let element: HTMLElement = document.getElementById('myFileInput') as HTMLElement;
    element.click();
  }

  fileChange(event){
    if(event.target.files && event.target.files[0]){
      let reader = new FileReader();

    reader.onload = (event:any) => {
      this.picture = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

}
